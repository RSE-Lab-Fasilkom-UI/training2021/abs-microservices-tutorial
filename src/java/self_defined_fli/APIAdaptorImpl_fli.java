package ABS.Framework.APIAdaptor;

import abs.backend.java.lib.types.ABSString;
import abs.backend.java.lib.types.ABSInteger;
import abs.backend.java.lib.types.ABSUnit;
import abs.backend.java.lib.types.ABSValue;
import ABS.StdLib.Pair;
import ABS.StdLib.Pair_Pair;
import abs.backend.java.lib.runtime.FLIHelper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.rse.middleware.DataTransformer;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.SystemDefaultRoutePlanner;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class APIAdaptorImpl_fli extends APIAdaptorImpl_c {
    @Override
    public ABS.StdLib.Map<ABSString, ABSString> fli_call_reqres_GET(ABS.StdLib.Map<ABSString, ABSString> payload) {
        // STR: convert input
        HashMap<String, String> param = DataTransformer.convertABSMapToJavaMap(payload);
        HashMap<String, String> headers = new HashMap<>();
        String endpoint = "";
        List<StringPair> urlParameters = new ArrayList<>();

        // convert to communication library payload
        for (Map.Entry<String, String> api_payload : param.entrySet()) {
        	String key = api_payload.getKey();
            String value = api_payload.getValue();
            System.out.println(key);
            System.out.println(value);

            if (key.equals("ENDPOINT")){
                endpoint = value;
            } else if (key.contains("HEADER")) {
                String headerKey = key.split("_")[1];
                headers.put(headerKey, value);
            } else {
                urlParameters.add(new StringPair(key, value));
            }
        }

        // build URI for get request
        String fullEndpoint = "";
        try {
            URIBuilder builder = new URIBuilder(endpoint);
            for (StringPair urlParameter : urlParameters) {
                builder.setParameter(urlParameter.first, urlParameter.second);
            }
            fullEndpoint = builder.build().toString();
        }catch(URISyntaxException e) {
            System.out.println(e.getMessage());
        }

        // STR: calling the api
        System.out.println("endpoint :"+fullEndpoint);
        String json_result = "";
        HttpGet get = new HttpGet(fullEndpoint);

        System.out.println("Proxy is setted to "+System.getProperty("http.proxyHost"));
        System.out.println("Proxy is setted to "+System.getProperty("https.proxyHost"));
        System.out.println("set request headers");
        for (Map.Entry<String, String> header : headers.entrySet()) {
            get.addHeader(header.getKey(), header.getValue());
        }
        try {
            System.out.println("creating closable http client");
            SystemDefaultRoutePlanner routePlanner = new SystemDefaultRoutePlanner(ProxySelector.getDefault());
            CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRoutePlanner(routePlanner).build();
            System.out.println("setting proxy");

            System.out.println("prepare to execute GET");
            CloseableHttpResponse response = httpClient.execute(get);
            System.out.println("prepare to get json result");
            json_result = EntityUtils.toString(response.getEntity());
        }catch(IOException e) {
        	System.out.println(e.getMessage());
        }
        System.out.println(json_result);

        // STR: convert output
        ABS.StdLib.Map<ABSString, ABSString> result = null;
        try {
            result = DataTransformer.convertJSONToABSMap(json_result);
        }catch (JSONException e) {
        	System.out.println(e.getMessage());
        }
        return result;
    }

	@Override
    public ABS.StdLib.Map<ABSString, ABSString> fli_call_reqres_POST(ABS.StdLib.Map<ABSString, ABSString> payload) {
        // STR: convert input
        HashMap<String, String> param = DataTransformer.convertABSMapToJavaMap(payload);
        HashMap<String, String> headers = new HashMap<>();
        String contentType = "";
        if (param.containsKey("HEADER_Content-Type")) {
            contentType = param.get("HEADER_Content-Type");
            System.out.println(contentType);
        }
        String endpoint = "";

        // dipakai pada content-type json
        JSONObject body = new JSONObject();

        // dipakai pada content-type generik
        List<StringPair> urlParameters = new ArrayList<>();

        // convert to communication library payload
        for (Map.Entry<String, String> api_payload : param.entrySet()) {
        	String key = api_payload.getKey();
            String value = api_payload.getValue();
            System.out.println(key);
            System.out.println(value);

            if (key.equals("ENDPOINT")){
                endpoint = value;
            } else if (key.contains("HEADER")) {
                String headerKey = key.split("_")[1];
                headers.put(headerKey, value);
            } else {
                if (contentType.equalsIgnoreCase("application/json")) {
                    System.out.println("Enter Body Insert");
                    putPairOnBody(key, value, body);
                }
                else {
                    urlParameters.add(new StringPair(key, value));
                }
            }
            System.out.println(body.toString());
        }

        // STR: calling the api
        System.out.println("endpoint :"+endpoint);
        System.out.println("url param: "+urlParameters);
        String json_result = "";
        HttpPost post = new HttpPost(endpoint);

        System.out.println("Proxy is setted to "+System.getProperty("http.proxyHost"));
        System.out.println("Proxy is setted to "+System.getProperty("https.proxyHost"));
        try {
            System.out.println("set entity with param");
            for (Map.Entry<String, String> header : headers.entrySet()) {
                post.addHeader(header.getKey(), header.getValue());
            }
            if (contentType.equalsIgnoreCase("application/json")) post.setEntity(new StringEntity(body.toString()));
            else post.setEntity(new UrlEncodedFormEntity(urlParameters));

        }catch(UnsupportedEncodingException e){
        	System.out.println(e.getMessage());
        }
        try {
            System.out.println("creating closable http client");
            SystemDefaultRoutePlanner routePlanner = new SystemDefaultRoutePlanner(ProxySelector.getDefault());
            CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRoutePlanner(routePlanner).build();
            System.out.println("setting proxy");

            System.out.println("prepare to execute POST");
            CloseableHttpResponse response = httpClient.execute(post);
            System.out.println("prepare to get json result");
            json_result = EntityUtils.toString(response.getEntity());
        }catch(IOException e) {
        	System.out.println(e.getMessage());
        }
        System.out.println(json_result);

        // STR: convert output
        ABS.StdLib.Map<ABSString, ABSString> result = null;
        try {
            result = DataTransformer.convertJSONToABSMap(json_result);
        }catch (JSONException e) {
        	System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public ABSString fli_call_reqres_GET_users(ABSString host, ABSInteger port, ABSString scheme, ABSString request) {
        System.out.println("call from api adaptor, get users");
        // convert tipe data
        String hostjava = host.getString();
        int portjava = port.toInt();
        String schemejava = scheme.getString();
        String requestjava = request.getString();

        // calling the API
        String result = "";
        SystemDefaultRoutePlanner routePlanner = new SystemDefaultRoutePlanner(ProxySelector.getDefault());
        CloseableHttpClient httpclient = HttpClientBuilder.create()
            .setRoutePlanner(routePlanner).build();
        System.out.println("Proxy is setted to "+System.getProperty("http.proxyHost"));
        System.out.println("Proxy is setted to "+System.getProperty("https.proxyHost"));
        try {
          // specify the host, protocol, and port
          HttpHost target = new HttpHost(hostjava, portjava, schemejava);
          // specify the get request
          HttpGet getRequest = new HttpGet(requestjava);
          System.out.println("executing request to " + target);
          HttpResponse httpResponse = httpclient.execute(target, getRequest);
          HttpEntity entity = httpResponse.getEntity();
          System.out.println("----------------------------------------");
          System.out.println(httpResponse.getStatusLine());
          Header[] headers = httpResponse.getAllHeaders();
          for (int i = 0; i < headers.length; i++) {
            System.out.println(headers[i]);
          }
          System.out.println("----------------------------------------");
          if (entity != null) {
            result = EntityUtils.toString(entity);
            System.out.println(result);
          }
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          // When HttpClient instance is no longer needed,
          // shut down the connection manager to ensure
          // immediate deallocation of all system resources
          httpclient.getConnectionManager().shutdown();
        }

        // convert balik tipe data
        return ABSString.fromString(result);
    }

    public void putWithNestedKey(String key, String value, JSONObject json) {
        System.out.println(key);
        System.out.println(json.toString());
        String[] splited_keys = key.split("\\.");
        for (String key_level: splited_keys) System.out.println(key_level);
        int lastIdx = splited_keys.length - 1;
        JSONObject currentJson = json;
        try {
            for (int i=0;i<lastIdx;i++) {
                String currentKey = splited_keys[i];
                System.out.println(currentKey);
                if (!currentJson.has(currentKey)) currentJson.put(currentKey, new JSONObject());
                currentJson = currentJson.getJSONObject(currentKey);
            }
            currentJson.put(splited_keys[lastIdx], value);
        } catch (JSONException e) {
        	System.out.println(e.getMessage());
        }
    }

    public void putPairOnBody(String key, String value, JSONObject body) {
        System.out.println(key);
        if (key.contains("NESTED")) {
            String nestedKey = key.split("_")[1];
            System.out.println("Enter Insert nested");
            putWithNestedKey(nestedKey, value, body);
        } else {
            try {
                body.put(key, value);
            } catch (JSONException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}

class StringPair extends BasicNameValuePair{
	public String first;
	public String second;
	public StringPair(String name, String value) {
		super(name, value);
		this.first = name;
		this.second = value;
		// TODO Auto-generated constructor stub
	}
}
